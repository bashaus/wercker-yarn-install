# Yarn Install

Wercker step to install node dependencies via yarn. Saves cache to the
`$WERCKER_CACHE_DIR` for persistence.

## Notes

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
RFC 2119.

## Sample Usage

    box: wercker/nodejs
    deploy:
      steps:
        - uk-hando/yarn-install

&nbsp;

## Step Properties

### clear-cache-on-failed

This script attempts to run `yarn install` three times before failing.
After each failure, should the yarn cache directory be cleared? Enable
with `true` or disable with `false`.

* Since: `0.0.1`
* Property is `Optional`
* Default value is: `true` (clear cache on failure)
* Recommended location: `Inline`
* `Validation` rules:
  * Must be either `true`, `false`, `1` or `0`

&nbsp;

### production

Whether to install development dependencies. Exclude `devDependencies` with
`true` or include with `false`. In versions prior to `0.1.0`, production was
`true` by default. In versions `0.1.0` and newer, production is `false`
by default.

* Since: `0.0.1`
* Property is `Optional`
* Default value is: `false` (include `devDependencies`)
* Recommended location: `Inline`
* `Validation` rules:
  * Must be either `true`, `false`, `1` or `0`

&nbsp;

### opts

Any additional options and flags that you would like to pass to yarn.

* Since: `0.0.1`
* Property is `Optional`
* Recommended location: `Inline`

&nbsp;
